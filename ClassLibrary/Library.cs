﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace Library
{
    // -----------------
    // ИСХОДНЫЕ ЗНАЧЕНИЯ
    // -----------------
    public class InputValues
    {
        // Некоторые постоянные значения, необходимые для расчётов
        public static double D_V = (Math.Pow(2.2, -5));    // Динамическая вязкость газа при рабочих условиях, Па с
        public static double I = 0.22; // Заданный средний ток короны, мА/м
        public static double K_F = 1.4;   // Коэффициент формы кривой тока [1.1 - 1.4]
        public static double K_E = 1.412; // Коэффициент перехода от амплитудного значения напряжения к эффективному [1.405 - 1.412]
        public static double P_EL = Math.Pow(10, 8);  // Удельное электрическое сопротивление слоя пыли, Ом * м 

        public InputValues(double _V_RO, double _X, double _T_GAZ, double _P_BAR, double _P_GI, double _W, List<double> _Q_J, double _Z)
        {
            V_RO = _V_RO;   // Расход сухого газа на очистку при нормальных условиях, м3/ч
            X = _X;         // Содержание водяных паров в газе, г/м3
            T_GAZ = _T_GAZ; // Температура очищаемого газа на входе, C
            P_BAR = _P_BAR; // Барометрическое давление на местности, кПа
            P_GI = _P_GI;   // Избыточное давление очищаемого газа, кПа
            W = _W;         // Рекомендуемая скорость очищаемого газа, м/c
            Q_J = _Q_J;     // Фракционный состав пыли, мкм
            Z = _Z;         // Концентрация пыли на входе, г/м3

        }

        public static double V_RO = 108000;  // Расход сухого газа на очистку при нормальных условиях, м3/ч
        public static double X = 100;        // Содержание водяных паров в газе, г/м3
        public static double T_GAZ = 300;    // Температура очищаемого газа на входе, C
        public static double P_BAR = 101.5;  // Барометрическое давление на местности, кПа
        public static double P_GI = 9.5;     // Избыточное давление очищаемого газа, кПа
        public static double W = 0.8;        // Рекомендуемая скорость очищаемого газа, м/c
        public static List<double> Q_J = new List<double>() { 2, 10, 15, 25, 50, 75 };  // Фракционный состав пыли, мкм
        public static double Z = 30;         // Концентрация пыли на входе, г/м3
    }

    // ----------------------
    // ПРОМЕЖУТОЧНЫЕ ЗНАЧЕНИЯ
    // ----------------------
    public class IntermediateValues
    {
        // Расход газа на очистку в рабочих условиях (V_GAZ)
        public static double GetExpenditureAmount()
        {
            return InputValues.V_RO * (1 + ((InputValues.X * (Math.Pow(10, -3))) / 0.804)) * (InputValues.T_GAZ + 273) * ((101.3) / (273 * (InputValues.P_BAR + InputValues.P_GI)));
        }

        // Площадь осаждения (F_PA)
        public static double GetDepositionArea()
        {
            return GetExpenditureAmount() / (3600 * InputValues.W);
        }
    }

    // -------------------------
    // ВЫБОР ТИПА ЭЛЕКТРОФИЛЬТРА
    // -------------------------
    public class TypeOfElectricFilter
    {
        // Подключение к базе данных Microsoft Access
        public static string connectString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Filters.mdb;";
        private static OleDbConnection myConnection;

        public static string FMark; // Марка электрофильтра
        public static double N_CK; // Число секций
        public static double N_G;  // Число газовых проходов
        public static double A_H;  // Активная высота электродов
        public static double N_P;  // Число полей

        public static double A_L;  // Активная длина поля
        public static double F_A;  // Площадь активного сечения
        public static double F_OC; // Общая площадь осаждения

        public static double F_L; // Длина
        public static double F_H; // Высота
        public static double F_B; // Ширина

        public static double R;  // Эффективный радиус коронирующего электрода
        public static double HK; // Шаг между одноимёнными электродами
        public static double HP; // Расстояние между коронирующим и осадительным электродами
        public static string COR_EL; // Тип коронирующего электрода

        public static double W_U; // Уточненная скорость газа в электрофильтре

        // Примечания к разным маркам фильтров
        public static void GetDefaultNotationOfIGA()
        {
            HK = 0.3;  // Шаг между одноимёнными электродами
            R = 2 * (Math.Pow(10, -3)); // Эффективный радиус коронирующего электрода, м
            COR_EL = "игольчатый"; // Тип коронирующего электрода
            HP = 0.133; // Расстояние между коронирующим и осадительным электродами
        }
        public static void GetDefaultNotationOfIGT()
        {
            // ПРИМЕЧАНИЯ К ЭЛЕКТРОФИЛЬТРАМ МАРКИ "ЭГТ"
            N_G = 1; // Число газовых проходов
            N_P = 1; // Число полей
            N_CK = 1; // Число секций
            A_H = 7.5; // Активная высота электродов 
            HK = 0.26; // Шаг между электродами
            R = 0.0011; // Эффективный радиус коронирующего электрода
            COR_EL = "гладкий"; // Тип коронирующего электрода
            HP = 0.1; // Расстояние между коронирующим и осадительным электродами
        }
        public static void GetDefaultNotationOfUV()
        {
            // ПРИМЕЧАНИЯ К ЭЛЕКТРОФИЛЬТРАМ МАРКИ "УВ"
            N_G = 1; // Число газовых проходов
            N_P = 1; // Число полей
            HK = 0.275; // Шаг между одноимёнными электродами
            R = 0.002; // Эффективный радиус коронирующего электрода
            COR_EL = "игольчатый"; // Тип коронирующего электрода
            HP = 0.131; // Расстояние между коронирующим и осадительным электродами
        }

        // Методы получения переменных из марки фильтра
        public static void GetDecryptionsOfNameFilterIGA(string filterName)
        {
            string[] filter = filterName.Split(new char[] { '-' });

            // List числовых значений, полученных из названия фильтра
            List<double> decryptionNumbers = new List<double>();

            bool isNameOfFilter = true;
            // Добавление значения в List
            foreach (string s in filter)
            {

                if (isNameOfFilter)
                {
                    FMark = s[0].ToString() + s[1].ToString() + s[2].ToString(); // Марка электрофильтра

                    string numSection = s[3].ToString();
                    double num = Convert.ToDouble(numSection);

                    decryptionNumbers.Add(num);
                }
                else
                {
                    double filterDecryption = Convert.ToDouble(s);
                    decryptionNumbers.Add(filterDecryption);
                }

                isNameOfFilter = false;
            }

            N_CK = Convert.ToDouble(decryptionNumbers[0]); // Число секций
            N_G = Convert.ToDouble(decryptionNumbers[1]); // Число газовых проходов
            A_H = Convert.ToDouble(decryptionNumbers[2]); // Активная высота электродов
            N_P = Convert.ToDouble(decryptionNumbers[4]); // Число полей
        }
        public static void GetDecryptionsOfNameFilterIGT(string filterName)
        {
            string[] filter = filterName.Split(new char[] { '-' });

            // List числовых значений, полученных из названия фильтра
            List<double> decryptionNumbers = new List<double>();

            bool isNameOfFilter = true;
            // Добавление значения в List
            foreach (string s in filter)
            {
                if (isNameOfFilter)
                {
                    FMark = s[0].ToString() + s[1].ToString() + s[2].ToString(); // Марка электрофильтра
                }
                else
                {
                    double filterDecryption = Convert.ToDouble(s);

                    decryptionNumbers.Add(filterDecryption);
                }

                isNameOfFilter = false;
            }

            N_P = Convert.ToDouble(decryptionNumbers[0]); // Число полей
            A_L = Convert.ToDouble(decryptionNumbers[1]); // Активная длина поля
            F_A = Convert.ToDouble(decryptionNumbers[2]); // Площадь активного сечения

        }
        public static void GetDecryptionsOfNameFilterUV(string filterName)
        {
            string[] filter = filterName.Split(new char[] { '-' });

            // List числовых значений, полученных из названия фильтра
            List<double> decryptionNumbers = new List<double>();

            bool isNameOfFilter = true;
            // Добавление значения в List
            foreach (string s in filter)
            {
                if (isNameOfFilter == true)
                {
                    FMark = s.ToString(); // Марка электрофильтра
                }
                else if (isNameOfFilter == false)
                {
                    string numSection = s[0].ToString();
                    string activeHeight = s[2].ToString() + s[3].ToString();
                    double numS = Convert.ToDouble(numSection);
                    double activeH = Convert.ToDouble(activeHeight);

                    decryptionNumbers.Add(numS);
                    decryptionNumbers.Add(activeH);
                }

                isNameOfFilter = false;
            }

            N_CK = Convert.ToDouble(decryptionNumbers[0]); // Число секций
            A_H = Convert.ToDouble(decryptionNumbers[1]); // Активная высота электродов

        }

        // Методы выбора фильтра в зависимости от расчётной площади активного сечения F_PA
        public static void SelectFilterOfIGA(double F_PA)
        {
            string query = "SELECT f_id, f_mark, f_active_length, f_square_active, f_square_total, f_overall_dim FROM IGA ORDER BY f_id";

            OleDbCommand command = new OleDbCommand(query, myConnection);
            OleDbDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                F_A = Convert.ToDouble(reader[3]);  // Площадь активного сечения

                if (F_A >= F_PA && F_PA >= 16.5) // 16.5 - минимальная площадь для фильтров "ЭГА"
                {
                    GetDefaultNotationOfIGA();

                    string filterName = reader[1].ToString();
                    GetDecryptionsOfNameFilterIGA(filterName);

                    A_L = Convert.ToDouble(reader[2]);  // Активная длина поля
                    F_OC = Convert.ToDouble(reader[4]); // Общая площадь осаждения

                    string[] filter = reader[5].ToString().Split(new char[] { 'х' });

                    F_L = Convert.ToDouble(filter[0]) * Math.Pow(10, -3); // Длина фильтра, м

                    // В данном случае округление реализовано специально для тестов (это не повлияет на конечный результат),
                    // поскольку при передаче значения в object происходит "баг".
                    // К примеру, значение "19.4" (19400) будет выглядеть как "19.0000000002"
                    F_H = Math.Round(Convert.ToDouble(filter[2]) * Math.Pow(10, -3), 1); // Высота фильтра, м

                    F_B = Convert.ToDouble(filter[1]) * Math.Pow(10, -3); // Ширина фильтра, м

                    break;
                }
                else
                {
                    F_A = 0;
                    HP = 0;
                }
            }

            reader.Close();
        }
        public static void SelectFilterOfIGT(double F_PA)
        {
            string query = "SELECT f_id, f_mark, f_square_total, f_overall_dim FROM IGT ORDER BY f_id";

            OleDbCommand command = new OleDbCommand(query, myConnection);
            OleDbDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                GetDecryptionsOfNameFilterIGT(reader[1].ToString());

                if (F_A >= F_PA && F_PA >= 20) // 20 - минимальная площадь для фильтров "ЭГТ"
                {
                    GetDefaultNotationOfIGT(); // Получаем значения из примечаний под таблицей (см. методичку)

                    F_OC = Convert.ToDouble(reader[2]); // Общая площадь осаждения

                    string[] filter = reader[3].ToString().Split(new char[] { 'x' });

                    F_L = Convert.ToDouble(filter[0]); // Длина фильтра, м
                    F_H = Convert.ToDouble(filter[2]); // Высота фильтра, м
                    F_B = Convert.ToDouble(filter[1]); // Ширина фильтра, м

                    break;
                }
                else
                {
                    FMark = "";
                    N_P = 0;
                    A_L = 0;
                    F_A = 0;
                    HP = 0;
                }
            }

            reader.Close();

        }
        public static void SelectFilterOfUV(double F_PA)
        {
            string query = "SELECT f_id, f_mark, f_square_active, f_square_total,  f_active_length, f_overall_dim FROM UV ORDER BY f_id";

            OleDbCommand command = new OleDbCommand(query, myConnection);
            OleDbDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                F_A = Convert.ToDouble(reader[2]);  // Площадь активного сечения

                if (F_A >= F_PA && F_PA >= 21) // 21 - минимальная площадь фильтров "УВ"
                {
                    GetDefaultNotationOfUV(); // Получаем значения из примечаний под таблицей (см. методичку)

                    string filterName = reader[1].ToString();
                    GetDecryptionsOfNameFilterUV(filterName);

                    A_L = Convert.ToDouble(reader[4]);  // Активная длина поля                    
                    F_OC = Convert.ToDouble(reader[3]); // Общая площадь осаждения

                    string[] filter = reader[5].ToString().Split(new char[] { 'х' });

                    F_L = Convert.ToDouble(filter[0]); // Длина фильтра, м
                    F_H = Convert.ToDouble(filter[2]); // Высота фильтра, м
                    F_B = Convert.ToDouble(filter[1]); // Ширина фильтра, м

                    break;
                }
                else
                {
                    F_A = 0;
                    HP = 0;
                }

            }

            reader.Close();
        }

        // МЕТОД ВЫБОРА ФИЛЬТРА (В ЗАВИСИМОСТИ ОТ ИСХОДНОЙ ТЕМПЕРАТУРЫ)
        public static void GetSelectedFilter()
        {
            // Подключение к базе данных Microsoft Access              
            myConnection = new OleDbConnection(connectString);
            myConnection.Open();

            // Максимально допустимые температуры газа для всех типов фильтров
            double T_MAX_IGA = 350;
            double T_MAX_IGT = 425;
            double T_MAX_UV = 250;

            // Площадь осаждения
            double F_PA = IntermediateValues.GetDepositionArea();

            if (InputValues.T_GAZ <= T_MAX_UV)
            {
                SelectFilterOfUV(F_PA);

            }
            else if (InputValues.T_GAZ <= T_MAX_IGA)
            {
                SelectFilterOfIGA(F_PA);

            }
            else if (InputValues.T_GAZ <= T_MAX_IGT)
            {
                SelectFilterOfIGT(F_PA);
            }
        }

        // Уточненная скорость газа в электрофильтре (W_U)
        public static double GetUpdatedSpeed()
        {
            return IntermediateValues.GetExpenditureAmount() / (3600 * F_A);
        }

    }

    // --------------------------------
    // РАСЧЁТ ПАРАМЕТРОВ ЭЛЕКТРОФИЛЬТРА
    // --------------------------------
    public class ParametersOfElectricFilter
    {
        // Расстояние между коронирующим и осадительным электродами (HP)
        public static double GetDistanceBetweenElectrodes()
        {
            // Вызов метода выбора фильтра для получения необходимых зачений
            TypeOfElectricFilter.GetSelectedFilter();

            return TypeOfElectricFilter.HP;
        }

        // Удельная осадительная поверхность (F_F)
        public static double GetPrecipitationSurface()
        {
            // Вызов метода выбора фильтра для получения необходимых зачений
            TypeOfElectricFilter.GetSelectedFilter();

            // Вспомогательные переменные (для наглядности)
            double W_U = TypeOfElectricFilter.GetUpdatedSpeed();

            return (TypeOfElectricFilter.N_P * TypeOfElectricFilter.A_L) / (TypeOfElectricFilter.HP * W_U);
        }

        // Время пребывания газа в электрофильтре (T_T)
        public static double GetGasResidenceTime()
        {
            // Вызов метода выбора фильтра для получения необходимых зачений
            TypeOfElectricFilter.GetSelectedFilter();

            // Вспомогательные переменные (для наглядности)
            double W_U = TypeOfElectricFilter.GetUpdatedSpeed();

            return TypeOfElectricFilter.A_L / W_U;
        }

        // Критическая напряженность электрического поля для коронирующего электрода отрицательной полярности (E_KP)
        public static double GetCriticalFieldOfNegativeElectrode()
        {
            // Вызов метода выбора фильтра для получения необходимых зачений
            TypeOfElectricFilter.GetSelectedFilter();

            double B = (273 / (101.3 * (InputValues.T_GAZ + 273))) * (InputValues.P_BAR + InputValues.P_GI); // Переменная для упрощения (повторяющееся значение)

            return 3.039 * (Math.Pow(10, 6)) * (B + 0.0311 * Math.Sqrt(B / TypeOfElectricFilter.R));
        }

        // Критическое напряжение электрического поля у коронирующего электрода (U_KP)
        public static double GetCriticaVoltageOfElectrode()
        {
            // Вызов метода выбора фильтра для получения необходимых зачений
            TypeOfElectricFilter.GetSelectedFilter();

            double D = 0; // Вспомогательная переменная
            double condition = TypeOfElectricFilter.HP / TypeOfElectricFilter.HK; // Вспомогательная переменная

            if (condition >= 1)
            {
                D = (TypeOfElectricFilter.HK / 2) * Math.Exp((Math.PI * TypeOfElectricFilter.HP) / TypeOfElectricFilter.HK);

            }
            else if (condition < 1)
            {

                D = (4 * TypeOfElectricFilter.HP) / Math.PI;
            }

            // Вспомогательные переменные (для наглядности)
            double U_KP = GetCriticalFieldOfNegativeElectrode();

            return U_KP * TypeOfElectricFilter.R * Math.Log(D / TypeOfElectricFilter.R);
        }

        // Напряженность электрического поля вблизи поверхности осаждения пыли (E_OC)
        public static double GetElectricFieldOfDepositionSurface()
        {
            return 5.48 * (Math.Pow(10, 5)) * Math.Sqrt(InputValues.I);
        }

        // Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций (W_W)
        public static List<double> GetPhysicalVelocityOnAnalyticalOfFractions()
        {
            // Вызов метода выбора фильтра для получения необходимых зачений
            TypeOfElectricFilter.GetSelectedFilter();

            // Коэффициенты дрейфа
            double a_1;
            double a_2;

            // Условие на тип коронирующего электрода
            if (TypeOfElectricFilter.COR_EL == "игольчатый")
            {
                a_1 = 3;
                a_2 = 0.2;

            }
            else
            {
                a_1 = 9;
                a_2 = 0.2;
            }

            // Вспомогательные переменные (для наглядности)
            double E_OC = GetElectricFieldOfDepositionSurface();
            double W_U = TypeOfElectricFilter.GetUpdatedSpeed();

            List<double> W_W = new List<double>();


            // Изначально первый множитель был равен 0.816, что расходилось с результатами контрольного примера (!)
            for (int i = 0; i < 6; i++)
            {
                W_W.Add(8.16 * E_OC * Math.Sqrt(InputValues.Q_J[i] * (a_1 * Math.Sqrt(InputValues.I * Math.Pow(10, -3)) * a_2 * W_U) * Math.Pow(10, -6) * ((1 / (4 * Math.PI * 9 * Math.Pow(10, 9))) / (InputValues.D_V))));
            }

            return W_W;
        }

        // Фракционная степень очистки газа (!) - расходится со значениями из контрольного примера - (Z_Z)
        public static List<double> GetFractionalDegreeOfPurification()
        {
            // Вызов метода выбора фильтра для получения необходимых зачений
            TypeOfElectricFilter.GetSelectedFilter();

            // Вспомогательные переменные (для наглядности)
            List<double> W_W = GetPhysicalVelocityOnAnalyticalOfFractions();
            double W_U = TypeOfElectricFilter.GetUpdatedSpeed();

            List<double> Z_Z = new List<double>();

            for (int i = 0; i < 6; i++)
            {
                Z_Z.Add(1 - Math.Exp((-TypeOfElectricFilter.N_P * W_W[i] * TypeOfElectricFilter.A_L) / (W_U * TypeOfElectricFilter.HP)));
            }

            return Z_Z;
        }

        // Общая аналитическая степень очистки газа (!) - расходится со значениями из контрольного примера - (ZZ_A)
        public static double GetGeneralAnalyticalDegreeOfPurification()
        {
            // Вспомогательные переменные (для наглядности)
            double summ = 0;

            List<double> Z_Z = GetFractionalDegreeOfPurification();

            // Принято решение расчёта среднего значения:
            for (int i = 0; i < Z_Z.Count; i++)
            {
                summ += Z_Z[i];
            }

            return summ / Z_Z.Count;
        }

        // Физическая скорость дрейфа частиц (W_E)
        public static double GetPhysicalSpeedOfParticles()
        {
            // Вспомогательные переменные (для наглядности)
            double summ = 0;

            List<double> W_W = GetPhysicalVelocityOnAnalyticalOfFractions();

            for (int i = 0; i < W_W.Count; i++)
            {
                summ += W_W[i];
            }

            return summ / W_W.Count;
        }

        // Количество осаждаемой пыли (M_OC)
        public static double GetAmountPrecipitationOfDust()
        {
            // Вспомогательыне переменные (для наглядности)
            double ZZ_A = GetGeneralAnalyticalDegreeOfPurification();
            double V_GAZ = IntermediateValues.GetExpenditureAmount();

            return Math.Pow(10, -3) * ZZ_A * InputValues.Z * V_GAZ;
        }

        // Количество уносимой из фильтра пыли (M_YH)
        public static double GetAmountRemovedOfDust()
        {
            // Вспомогательыне переменные (для наглядности)
            double ZZ_A = GetGeneralAnalyticalDegreeOfPurification();
            double V_GAZ = IntermediateValues.GetExpenditureAmount();

            return Math.Pow(10, -3) * (1 - ZZ_A) * InputValues.Z * V_GAZ;
        }

        // Рациональная пылеемкость осадительных электродов (M_P)
        public static double GetRationalDustOfElectrodes()
        {
            return 3.2 - 0.267 * Math.Log10(InputValues.P_EL);
        }

        // Рекомендуемое время между очередным встряхиванием осадительных электродов (T_P)
        public static double GetRecommendedTimeBetweenShakeOfElectrodes()
        {
            // Вызов метода выбора фильтра для получения необходимых зачений
            TypeOfElectricFilter.GetSelectedFilter();

            // Вспомогательыне переменные (для наглядности)
            double ZZ_A = GetGeneralAnalyticalDegreeOfPurification();
            double V_GAZ = IntermediateValues.GetExpenditureAmount();
            double M_P = GetRationalDustOfElectrodes();

            return (16.7 * TypeOfElectricFilter.F_OC * M_P) / (V_GAZ * InputValues.Z * ZZ_A);
        }

        // Сила тока, необходимая на питание одного поля электрофильтра (I_CUR)
        public static double GetRequiredCurrentToPowerOneFieldOfFilter()
        {
            // Вызов метода выбора фильтра для получения необходимых зачений
            TypeOfElectricFilter.GetSelectedFilter();

            return InputValues.I * ((TypeOfElectricFilter.A_H * TypeOfElectricFilter.A_L * TypeOfElectricFilter.N_G) / (TypeOfElectricFilter.HK));
        }
    }

    // ----------------------
    // ВЫБОР АГРЕГАТА ПИТАНИЯ
    // ----------------------
    public class TypeOfPowerUnit
    {
        // Подключение к базе данных Microsoft Access
        public static string connectString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Filters.mdb;";
        private static OleDbConnection myConnection;

        public static string PMark; // Марка питания агрегата
        public static double U_MAX;  // Выпрямленное напряжение (максимальное)
        public static double U_U;    // Выпрямленное напряжение (среднее)
        public static double I_L;    // Выпрямленный ток 
        public static double I_P;    // Потребляемый ток 
        public static double U_C;    // Напряжение сети 
        public static double N_C;    // Потребляемая из сети мощность
        public static double P_AG;   // КПД агрегата питания
        public static double K_N;    // Коэффициент мощности

        public static void SelectPowerUnitOfATG()
        {
            // Подключение к базе данных Microsoft Access              
            myConnection = new OleDbConnection(connectString);
            myConnection.Open();

            string query = "SELECT p_id, p_mark, p_rectified_voltage, p_average_rectified_current, p_current, p_voltage, p_power, p_efficiency_factor, p_power_coefficient FROM ATF ORDER BY p_id";

            OleDbCommand command = new OleDbCommand(query, myConnection);
            OleDbDataReader reader = command.ExecuteReader();

            // Вызов метода выбора фильтра для получения необходимых зачений
            TypeOfElectricFilter.GetSelectedFilter();

            // Вспомогательные переменные (для наглядности)
            double I_CUR = ParametersOfElectricFilter.GetRequiredCurrentToPowerOneFieldOfFilter();

            while (reader.Read())
            {
                if (Convert.ToDouble(reader[3]) >= I_CUR)
                {
                    PMark = reader[1].ToString();

                    string rectifiedVoltage = reader[2].ToString();
                    string[] rectifiedVoltageNumbers = rectifiedVoltage.Split(new char[] { '/' });

                    U_MAX = Convert.ToDouble(rectifiedVoltageNumbers[0]) * Math.Pow(10, 3);
                    U_U = Convert.ToDouble(rectifiedVoltageNumbers[1]) * Math.Pow(10, 3);

                    I_L = Convert.ToDouble(reader[3].ToString());
                    I_P = Convert.ToDouble(reader[4].ToString());

                    string voltage = reader[5].ToString();
                    string[] voltageNumbers = voltage.Split(new char[] { ',' });

                    U_C = Convert.ToDouble(voltageNumbers[0]);

                    N_C = Convert.ToDouble(reader[6].ToString());
                    P_AG = Convert.ToDouble(reader[7].ToString());
                    K_N = Convert.ToDouble(reader[8].ToString());
                    break;
                }
            }

            reader.Close();
        }
    }

    // ----------------------------------
    // РАСЧЁТ ПАРАМЕТРОВ АГРЕГАТА ПИТАНИЯ
    // ----------------------------------
    public class ParametersOfPowerUnit
    {
        // Расчетная потребляемая мощность агрегата на очистку газа (N_R)
        public static double GetEstimatePowerOfConsumption()
        {
            // Вызов метода выбора агрегата питания для получения необходимых значений
            TypeOfPowerUnit.SelectPowerUnitOfATG();

            // Вспомогательные переменные (для наглядности)
            double I_CUR = ParametersOfElectricFilter.GetRequiredCurrentToPowerOneFieldOfFilter();

            double N_NOM = TypeOfPowerUnit.U_U * TypeOfPowerUnit.I_L * Math.Pow(10, -6); // Номинальная мощность
            double K_I = I_CUR / TypeOfPowerUnit.I_L; // Коэффициент загрузки агрегата по току
            double K_U = TypeOfPowerUnit.U_MAX / TypeOfPowerUnit.U_U; // Коэффициент загрузки агрегата по напряжению

            return K_I * K_U * N_NOM;
        }

        // Общая мощность, потребляемая агрегатом питания (N_S)
        public static double GetTotalPowerOfConsumption()
        {
            // Вызов метода выбора агрегата питания для получения необходимых значений
            TypeOfPowerUnit.SelectPowerUnitOfATG();

            // Вспомогательные переменные (для наглядности)
            double N_R = GetEstimatePowerOfConsumption();

            // Вспомогательные переменные
            double N_ALL = 0.05 * N_R;


            return (TypeOfPowerUnit.U_MAX * TypeOfPowerUnit.I_L * InputValues.K_F * TypeOfPowerUnit.K_N / (InputValues.K_E * TypeOfPowerUnit.P_AG) + N_ALL) * Math.Pow(10, -6);
        }

        // Количество агрегатов питания на электрофильтр (N_A)
        public static double GetNumberOfAggregates()
        {
            // Вызов метода выбора фильтра для получения необходимых зачений
            TypeOfElectricFilter.GetSelectedFilter();

            return TypeOfElectricFilter.N_P;
        }
    }
}
