using NUnit.Framework;
using System;
using static Library.IntermediateValues;
using static Library.TypeOfElectricFilter;
using static Library.ParametersOfElectricFilter;
using static Library.TypeOfPowerUnit;
using static Library.ParametersOfPowerUnit;
using System.Collections.Generic;

namespace NUnitTestProject_Test
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestExpenditureAmount()
        {
            double expected = 10;
            double result = GetExpenditureAmount();

            Assert.AreEqual(expected, result);
        }
    }
}