﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static Library.IntermediateValues;
using static Library.TypeOfElectricFilter;
using static Library.ParametersOfElectricFilter;
using static Library.TypeOfPowerUnit;
using static Library.ParametersOfPowerUnit;
using System.Collections.Generic;
using System.Linq;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest
    {
        static string filename = "Calculation.xlsx";
        ExcelClass test = new ExcelClass(filename);


        // Расход газа на очистку в рабочих условиях (V_GAZ)
        [TestMethod]
        public void TestExpenditureAmount()
        {                  
            double expected = test.ExcelIntermediatetData[0];
            double result = GetExpenditureAmount();

            Assert.AreEqual(expected, result);
        }

        // Площадь осаждения (F_PA)
        [TestMethod]
        public void TestDepositionArea()
        {
            double expected = test.ExcelIntermediatetData[1];
            double result = GetDepositionArea();

            Assert.AreEqual(expected, result);
        }

        // Выбор типа электрофильтра
        [TestMethod]
        public void TestSelectedFilter()
        {
            // Вызов метода выбора типа электрофильтра
            GetSelectedFilter();

            object[] FilterValues = {
                        FMark, // Марка электрофильтра
                        N_CK, // Число секций
                        N_G,  // Число газовых проходов
                        A_H,  // Активная высота электродов
                        A_L,  // Активная длина поля
                        N_P,  // Число полей
                        F_A,  // Площадь активного сечения
                        HK,   // Шаг между одноимёнными электродами
                        F_OC, // Общая площадь осаждения
                        F_L, // Длина
                        F_H, // Высота
                        F_B, // Ширина
                        R,  // Эффективный радиус коронирующего электрода
                        W_U = GetUpdatedSpeed(), // Уточненная скорость газа в электрофильтре
                        COR_EL // Тип коронирующего электрода
                    };

            var expected = test.ExcelFilterData;
            var result = FilterValues;

            CollectionAssert.AreEqual(expected, result);
        }

        // Расстояние между коронирующим и осадительным электродами (HP)
        [TestMethod]
        public void TestDistanceBetweenElectrodes()
        {
            double expected = test.ExcelFilterParametersData[0];
            double result = GetDistanceBetweenElectrodes();

            Assert.AreEqual(expected, result);
        }

        // Удельная осадительная поверхность (F_F)
        [TestMethod]
        public void TestPrecipitationSurface()
        {
            double expected = test.ExcelFilterParametersData[1];
            double result = GetPrecipitationSurface();

            Assert.AreEqual(expected, result);
        }

        // Время пребывания газа в электрофильтре (T_T)
        [TestMethod]
        public void TestGasResidenceTime()
        {
            double expected = test.ExcelFilterParametersData[2];
            double result = GetGasResidenceTime();

            Assert.AreEqual(expected, result);
        }

        // Критическая напряженность электрического поля для коронирующего электрода отрицательной полярности (E_KP)
        [TestMethod]
        public void TestCriticalFieldOfNegativeElectrode()
        {
            double expected = test.ExcelFilterParametersData[3];
            double result = GetCriticalFieldOfNegativeElectrode();

            Assert.AreEqual(expected, result);
        }

        // Критическое напряжение электрического поля у коронирующего электрода (U_KP)
        [TestMethod]
        public void TestCriticaVoltageOfElectrode()
        {
            double expected = test.ExcelFilterParametersData[4];
            double result = GetCriticaVoltageOfElectrode();

            Assert.AreEqual(expected, result);
        }

        // Напряженность электрического поля вблизи поверхности осаждения пыли (E_OC)
        [TestMethod]
        public void TestElectricFieldOfDepositionSurface()
        {
            double expected = test.ExcelFilterParametersData[5];
            double result = GetElectricFieldOfDepositionSurface();

            Assert.AreEqual(expected, result);
        }

        // Физическая скорость дрейфа частиц по аналитическим данным для заданных фракций (W_W)
        [TestMethod]
        public void TestPhysicalVelocityOnAnalyticalOfFractions()
        {
            List<double> expected = new List<double>();

            for (int i = 6; i < 12; i++)
            {
                expected.Add(test.ExcelFilterParametersData[i]);
            }

            List<double> result = GetPhysicalVelocityOnAnalyticalOfFractions();

            CollectionAssert.AreEqual(expected, result);
        }

        // Фракционная степень очистки газа (Z_Z)
        [TestMethod]
        public void TestFractionalDegreeOfPurification()
        {
            List<double> expected = new List<double>();

            for (int i = 12; i < 18; i++)
            {
                expected.Add(test.ExcelFilterParametersData[i]);
            }

            List<double> result = GetFractionalDegreeOfPurification();

            CollectionAssert.AreEqual(expected, result);
        }

        // Общая аналитическая степень очистки газа (ZZ_A)
        [TestMethod]
        public void TestGeneralAnalyticalDegreeOfPurification()
        {
            double expected = test.ExcelFilterParametersData[18];
            double result = GetGeneralAnalyticalDegreeOfPurification();

            Assert.AreEqual(expected, result);
        }

        // Физическая скорость дрейфа частиц (W_E)
        [TestMethod]
        public void TestPhysicalSpeedOfParticles()
        {
            double expected = test.ExcelFilterParametersData[19];
            double result = GetPhysicalSpeedOfParticles();

            Assert.AreEqual(expected, result);
        }

        // Количество осаждаемой пыли (M_OC)
        [TestMethod]
        public void TestAmountPrecipitationOfDust()
        {
            double expected = test.ExcelFilterParametersData[20];

            // В данном случае округление оправдано лимитом знаков в Microsoft Excel
            double result = Math.Round(GetAmountPrecipitationOfDust(), 11);

            Assert.AreEqual(expected, result);
        }

        // Количество уносимой из фильтра пыли (M_YH)
        [TestMethod]
        public void TestAmountRemovedOfDust()
        {
            // В данном случае округление оправдано различием на 1 миллионую
            double expected = Math.Round(test.ExcelFilterParametersData[21], 11);
            double result = Math.Round(GetAmountRemovedOfDust(), 11);

            Assert.AreEqual(expected, result);
        }

        // Рациональная пылеемкость осадительных электродов (M_P)
        [TestMethod]
        public void TestRationalDustOfElectrodes()
        {
            double expected = test.ExcelFilterParametersData[22];
            double result = GetRationalDustOfElectrodes();

            Assert.AreEqual(expected, result);
        }

        // Рекомендуемое время между очередным встряхиванием осадительных электродов (T_P)
        [TestMethod]
        public void TestRecommendedTimeBetweenShakeOfElectrodes()
        {
            double expected = test.ExcelFilterParametersData[23];
            double result = GetRecommendedTimeBetweenShakeOfElectrodes();

            Assert.AreEqual(expected, result);
        }

        // Сила тока, необходимая на питание одного поля электрофильтра (I_CUR)
        [TestMethod]
        public void TestRequiredCurrentToPowerOneFieldOfFilter()
        {
            double expected = test.ExcelFilterParametersData[24];
            double result = GetRequiredCurrentToPowerOneFieldOfFilter();

            Assert.AreEqual(expected, result);
        }

        // Выбор агрегата питанимя
        [TestMethod]
        public void TestSelectPowerUnit()
        {
            // Вызов метода выбора агрегата питания
            SelectPowerUnitOfATG();

            object[] FilterValues =
            {
                        PMark, // Марка питания агрегата
                        U_MAX,  // Выпрямленное напряжение (максимальное)
                        U_U,    // Выпрямленное напряжение (среднее)
                        I_L,    // Выпрямленный ток 
                        I_P,    // Потребляемый ток 
                        U_C,    // Напряжение сети 
                        N_C,    // Потребляемая из сети мощность
                        P_AG,   // КПД агрегата питания
                        K_N    // Коэффициент мощности
                    };

            var expected = test.ExcelPowerUnitData;
            var result = FilterValues;

            CollectionAssert.AreEqual(expected, result);
        }

        // Расчетная потребляемая мощность агрегата на очистку газа (N_R)
        [TestMethod]
        public void TestEstimatePowerOfConsumption()
        {
            double expected = test.ExcelPowerUnitParametersData[0];
            double result = GetEstimatePowerOfConsumption();

            Assert.AreEqual(expected, result);
        }

        // Общая мощность, потребляемая агрегатом питания (N_S)
        [TestMethod]
        public void TestTotalPowerOfConsumption()
        {
            double expected = test.ExcelPowerUnitParametersData[1];
            double result = GetTotalPowerOfConsumption();

            Assert.AreEqual(expected, result);
        }

        // Количество агрегатов питания на электрофильтр (N_A)
        [TestMethod]
        public void TestNumberOfAggregates()
        {
            double expected = test.ExcelPowerUnitParametersData[2];
            double result = GetNumberOfAggregates();

            Assert.AreEqual(expected, result);
        }
    }
}
