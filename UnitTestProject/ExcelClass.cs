﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop;


namespace UnitTestProject
{
    class ExcelClass
    {
        public ExcelClass(string path)
        {
            ExcelRead(path);
        }

        // Массив промежуточных значений
        public double[] ExcelIntermediatetData;

        // Массив значений электрофильтра
        public object[] ExcelFilterData;

        // Массив параметров электрофильтра
        public double[] ExcelFilterParametersData;

        // Массив значений агрегата питания
        public object[] ExcelPowerUnitData;

        // Массив параметров агрегата питания
        public double[] ExcelPowerUnitParametersData;

        public void ExcelRead(string filename)
        {
            // Подключение к Microsoft Excel
            Application ex = new Application();
            Workbook exBook = ex.Workbooks.OpenXML(AppDomain.CurrentDomain.BaseDirectory + @"\" + filename);
            Worksheet exSheets;
            exSheets = (Worksheet)exBook.Sheets[1];

            // Промежуточные значения
            Microsoft.Office.Interop.Excel.Range IntermediateResults = exSheets.UsedRange.Range[exSheets.Cells[28, 8], exSheets.Cells[29, 8]] as Microsoft.Office.Interop.Excel.Range;
            Array IntermediateResultsArray = (Array)IntermediateResults.Cells.Value2;
            ExcelIntermediatetData = IntermediateResultsArray.OfType<object>().Select(o => Convert.ToDouble(o)).ToArray();

            // Выбор типа электрофильтра
            Microsoft.Office.Interop.Excel.Range FilterResults = exSheets.UsedRange.Range[exSheets.Cells[33, 8], exSheets.Cells[47, 8]] as Microsoft.Office.Interop.Excel.Range;
            Array FilterResultsArray = (Array)FilterResults.Cells.Value2;
            ExcelFilterData = FilterResultsArray.Cast<object>().ToArray();

            // Параметры электрофильтра
            Microsoft.Office.Interop.Excel.Range FilterParametersResults = exSheets.UsedRange.Range[exSheets.Cells[51, 8], exSheets.Cells[83, 8]] as Microsoft.Office.Interop.Excel.Range;
            Array FilterParametersArray = (Array)FilterParametersResults.Cells.Value2;
            ExcelFilterParametersData = FilterParametersArray.OfType<object>().Select(o => Convert.ToDouble(o)).ToArray();

            // Выбор агрегата питания
            Microsoft.Office.Interop.Excel.Range PowerUnitResults = exSheets.UsedRange.Range[exSheets.Cells[87, 8], exSheets.Cells[95, 8]] as Microsoft.Office.Interop.Excel.Range;
            Array PowerUnitArray = (Array)PowerUnitResults.Cells.Value2;
            ExcelPowerUnitData = PowerUnitArray.Cast<object>().ToArray();

            // Параметры агрегата питания
            Microsoft.Office.Interop.Excel.Range PowerUnitParametersResults = exSheets.UsedRange.Range[exSheets.Cells[99, 8], exSheets.Cells[102, 8]] as Microsoft.Office.Interop.Excel.Range;
            Array PowerUnitParametersArray = (Array)PowerUnitParametersResults.Cells.Value2;
            ExcelPowerUnitParametersData = PowerUnitParametersArray.OfType<object>().Select(o => Convert.ToDouble(o)).ToArray();

            ex.Workbooks.Close();
        }

    }
}
